package main

import (
	"flag"
	"fmt"
	"math/rand"
)

func heap(data []int, k int) {
	if k == 1 {
		fmt.Println(data)
		return
	}

	heap(data, k-1)

	for i := 0; i < k-1; i++ {
		if (k % 2) == 0 {
			// even
			data[i], data[k-1] = data[k-1], data[i]
		} else {
			data[0], data[k-1] = data[k-1], data[0]
		}
		heap(data, k-1)
	}
}

func permute(data []int) {
	heap(data, len(data))
}

func main() {
	size := flag.Int("size", 4, "data set size")
	flag.Parse()

	data := rand.Perm(*size)
	permute(data)
}
