package main

import (
	"flag"
	"fmt"
	"math/rand"
)

func shuffle_fisher_yates(data []int) {
	l := len(data)
	for i := 0; i < l; i++ {
		r := l - i
		j := int(rand.Int31n(int32(r))) + i
		data[j], data[i] = data[i], data[j]
	}
}

func main() {
	size := flag.Int("size", 4, "data set size")
	flag.Parse()

	data := rand.Perm(*size)
	for {
		fmt.Println(data)
		shuffle_fisher_yates(data)
	}
}
