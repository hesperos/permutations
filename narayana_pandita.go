package main

import (
	"flag"
	"fmt"
	"math/rand"
)

func permute_narayana_pandita(data []int) (isLast bool) {
	isLast = false
	k := -1
	n := len(data)

	// find the biggest k where data[k+1] > data[k]
	for i := n - 2; i >= 0; i-- {
		if data[i+1] > data[i] {
			k = i
			break
		}
	}

	// given sequence has no further permutations
	if k == -1 {
		isLast = true
		return
	}

	// find largest l (where l > k) such that data[k] < data[l], and swap the values
	for l := n - 1; l > k; l-- {
		if data[l] > data[k] {
			data[l], data[k] = data[k], data[l]
			break
		}
	}

	// reverse the sequence from k + 1 to n
	right := n - 1
	for left := k + 1; left < (n-k)/2; left++ {
		data[left], data[right] = data[right], data[left]
		right--
	}

	return
}

func main() {
	size := flag.Int("size", 4, "data set size")
	flag.Parse()

	data := rand.Perm(*size)
	for {
		fmt.Println(data)
		if permute_narayana_pandita(data) {
			break
		}
	}
}
